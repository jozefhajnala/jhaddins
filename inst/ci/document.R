docOrg <- lapply(c(dir("man", full.names = TRUE), "NAMESPACE"), readLines)
devtools::document()
docNew <- lapply(c(dir("man", full.names = TRUE), "NAMESPACE"), readLines)
if (!identical(docOrg, docNew)) {
  stop("Documentation was not updated, try running devtools::document().")
}
