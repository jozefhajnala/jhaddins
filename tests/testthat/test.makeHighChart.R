context("makeHighChart")

# The hc is equivalent to the hc in example:
# hc <- highcharter::hc_add_series(
#   highcharter::highchart(),
#   data = datasets::mtcars$mpg
# )

hc <- structure(
  list(
    x = structure(
      list(
        hc_opts = structure(
          list(
            title = structure(list(text = NULL), .Names = "text"),
            yAxis = structure(list(title = structure(
              list(text = NULL),
              .Names = "text"
            )), .Names = "title"),
            credits = structure(list(enabled = FALSE), .Names = "enabled"),
            exporting = structure(list(enabled = FALSE), .Names = "enabled"),
            plotOptions = structure(
              list(
                series = list(turboThreshold = 0),
                treemap = list(layoutAlgorithm = "squarified"),
                bubble = list(minSize = 5, maxSize = 25)
              ),
              .Names = c("series", "treemap", "bubble")
            ),
            annotationsOptions = list(enabledButtons = FALSE),
            tooltip = list(delayForDisplay = 10),
            series = list(structure(list(
              data = c(
                21,
                21,
                22.8,
                21.4,
                18.7,
                18.1,
                14.3,
                24.4,
                22.8,
                19.2,
                17.8,
                16.4,
                17.3,
                15.2,
                10.4,
                10.4,
                14.7,
                32.4,
                30.4,
                33.9,
                21.5,
                15.5,
                15.2,
                13.3,
                19.2,
                27.3,
                26,
                30.4,
                15.8,
                19.7,
                15,
                21.4
              )
            ), .Names = "data"))
          ),
          .Names = c(
            "title",
            "yAxis",
            "credits",
            "exporting",
            "plotOptions",
            "annotationsOptions",
            "tooltip",
            "series"
          )
        ),
        theme = structure(list(chart = structure(
          list(backgroundColor = "transparent"),
          .Names = "backgroundColor"
        )), .Names = "chart", class = "hc_theme"),
        conf_opts = structure(
          list(
            global = list(
              Date = NULL,
              getTimezoneOffset = NULL,
              timezoneOffset = 0,
              useUTC = TRUE
            ),
            lang = list(
                contextButtonTitle = "Chart context menu",
                decimalPoint = ".",
                downloadJPEG = "Download JPEG image",
                downloadPDF = "Download PDF document",
                downloadPNG = "Download PNG image",
                downloadSVG = "Download SVG vector image",
                drillUpText = "Back to {series.name}",
                invalidDate = NULL,
                loading = "Loading...",
                months = c(
                  "January",
                  "February",
                  "March",
                  "April",
                  "May",
                  "June",
                  "July",
                  "August",
                  "September",
                  "October",
                  "November",
                  "December"
                ),
                noData = "No data to display",
                numericSymbols = c(
                  "k",
                  "M", "G", "T", "P", "E"
                ),
                printChart = "Print chart",
                resetZoom = "Reset zoom",
                resetZoomTitle = "Reset zoom level 1:1",
                shortMonths = c(
                  "Jan",
                  "Feb",
                  "Mar",
                  "Apr",
                  "May",
                  "Jun",
                  "Jul",
                  "Aug",
                  "Sep",
                  "Oct",
                  "Nov",
                  "Dec"
                ),
                thousandsSep = " ",
                weekdays = c(
                  "Sunday",
                  "Monday",
                  "Tuesday",
                  "Wednesday",
                  "Thursday",
                  "Friday",
                  "Saturday"
                )
              )
          ),
          .Names = c(
            "global",
            "lang"
          )
        ),
        type = "chart",
        fonts = character(0),
        debug = FALSE
      ),
      .Names = c(
        "hc_opts",
        "theme", "conf_opts", "type", "fonts", "debug"
      ),
      TOJSON_ARGS = structure(list(pretty = FALSE), .Names = "pretty")
    ),
    width = NULL,
    height = NULL,
    sizingPolicy = structure(
      list(
        defaultWidth = "100%",
        defaultHeight = NULL,
        padding = NULL,
        viewer = structure(
          list(
            defaultWidth = NULL,
            defaultHeight = NULL,
            padding = NULL,
            fill = TRUE,
            suppress = FALSE,
            paneHeight = NULL
          ),
          .Names = c(
            "defaultWidth",
            "defaultHeight",
            "padding",
            "fill",
            "suppress",
            "paneHeight"
          )
        ),
        browser = structure(
          list(
            defaultWidth = NULL,
            defaultHeight = NULL,
            padding = NULL,
            fill = TRUE,
            external = FALSE
          ),
          .Names = c(
            "defaultWidth",
            "defaultHeight", "padding", "fill", "external"
          )
        ),
        knitr = structure(
          list(
            defaultWidth = "100%",
            defaultHeight = NULL,
            figure = FALSE
          ),
          .Names = c(
            "defaultWidth",
            "defaultHeight", "figure"
          )
        )
      ),
      .Names = c(
        "defaultWidth",
        "defaultHeight",
        "padding",
        "viewer",
        "browser",
        "knitr"
      )
    ),
    dependencies = NULL,
    elementId = NULL,
    preRenderHook = NULL,
    jsHooks = list()
  ),
  .Names = c(
    "x",
    "width",
    "height",
    "sizingPolicy",
    "dependencies",
    "elementId",
    "preRenderHook",
    "jsHooks"
  ),
  class = c(
    "highchart",
    "htmlwidget"
  ),
  package = "highcharter"
)

test_that(
  "Simple highchart with defaults works",
  expect_equal(
    makeHighChart(hc),
    c(
      "<script type=\"text/javascript\">",


      paste(
        c(
          "$(function () {",
          "  $('#container').highcharts({",
          "  title: {     ",
          "    text: null     ",
          "  },     ",
          "  yAxis: {     ",
          "    title: {     ",
          "      text: null     ",
          "    }     ",
          "  },     ",
          "  credits: {     ",
          "    enabled: false     ",
          "  },     ",
          "  exporting: {     ",
          "    enabled: false     ",
          "  },     ",
          "  plotOptions: {     ",
          "    series: {     ",
          "      turboThreshold: 0     ",
          "    },     ",
          "    treemap: {     ",
          "      layoutAlgorithm: \"squarified\"     ",
          "    },     ",
          "    bubble: {     ",
          "      minSize: 5,     ",
          "      maxSize: 25     ",
          "    }     ",
          "  },     ",
          "  annotationsOptions: {     ",
          "    enabledButtons: false     ",
          "  },     ",
          "  tooltip: {     ",
          "    delayForDisplay: 10     ",
          "  },     ",
          "  series: [     ",
          "    {     ",
          paste0(
            "      data: [21, 21, 22.8, 21.4, 18.7, 18.1, 14.3, 24.4, 22.8,",
            " 19.2, 17.8, 16.4, 17.3, 15.2, 10.4, 10.4, 14.7, 32.4, 30.4,",
            " 33.9, 21.5, 15.5, 15.2, 13.3, 19.2, 27.3, 26, 30.4, 15.8, ",
            "19.7, 15, 21.4]     "
          ),
          "    }     ",
          "  ]     ",
          "}     ",
          "  );",
          "});"
        )
        ,
        collapse = "\n"
      ),


      "</script>",
      "",
      "<div id=\"container\"></div>"
    )
  )
)

test_that(
  "Simple highchart with cat works",
  expect_equal(
    capture.output(makeHighChart(hc, docat = TRUE)),
    c(
      "<script type=\"text/javascript\">",
      "$(function () {",
      "  $('#container').highcharts({",
      "  title: {     ",
      "    text: null     ",
      "  },     ",
      "  yAxis: {     ",
      "    title: {     ",
      "      text: null     ",
      "    }     ",
      "  },     ",
      "  credits: {     ",
      "    enabled: false     ",
      "  },     ",
      "  exporting: {     ",
      "    enabled: false     ",
      "  },     ",
      "  plotOptions: {     ",
      "    series: {     ",
      "      turboThreshold: 0     ",
      "    },     ",
      "    treemap: {     ",
      "      layoutAlgorithm: \"squarified\"     ",
      "    },     ",
      "    bubble: {     ",
      "      minSize: 5,     ",
      "      maxSize: 25     ",
      "    }     ",
      "  },     ",
      "  annotationsOptions: {     ",
      "    enabledButtons: false     ",
      "  },     ",
      "  tooltip: {     ",
      "    delayForDisplay: 10     ",
      "  },     ",
      "  series: [     ",
      "    {     ",
      paste0(
        "      data: [21, 21, 22.8, 21.4, 18.7, 18.1, 14.3, 24.4, 22.8,",
        " 19.2, 17.8, 16.4, 17.3, 15.2, 10.4, 10.4, 14.7, 32.4, 30.4,",
        " 33.9, 21.5, 15.5, 15.2, 13.3, 19.2, 27.3, 26, 30.4, 15.8, ",
        "19.7, 15, 21.4]     "
      ),
      "    }     ",
      "  ]     ",
      "}     ",
      "  );",
      "});",
      "</script>",
      "",
      "<div id=\"container\"></div>"
    )
  )
)
