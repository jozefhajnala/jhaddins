---
title: "How to create professional reports from R scripts, with custom styles"
description: "Useful tips on advanced options for styling, using themes and producing light-weight HTML reports directly from R scripts."
author: "Jozef Hajnala"
date: 2019-03-16T12:00:00Z
categories: ["vaRious"]
tags: ["r", "rmarkdown"]
---



<div id="introduction" class="section level1">
<h1>Introduction</h1>
<p>If the <a href="https://jozef.io/r909-rmarkdown-tips/">practical tips for R Markdown</a> post we talked briefly about how we can easily create professional reports directly from R scripts, without the need for converting them manually to Rmd and creating code chunks. In this one, we will provide useful tips on advanced options for styling, using themes and producing light-weight HTML reports directly from R scripts. We will also provide a repository with example R script and rendering code to get different styled and sized outputs easily.</p>
</div>
<div id="contents" class="section level1">
<h1>Contents</h1>
<ol style="list-style-type: decimal">
<li><a href="#creating-reports-directly-from-r-scripts">Creating reports directly from R scripts</a></li>
<li><a href="#using-knitrs-spin-directly">Using knitr’s spin directly</a></li>
<li><a href="#using-rmarkdowns-render">Using rmarkdown’s render</a></li>
<li><a href="#tldr-just-show-me-the-examples">TL;DR: Just show me the examples</a></li>
<li><a href="#references">References</a></li>
</ol>
</div>
<div id="creating-reports-directly-from-r-scripts" class="section level1">
<h1>Creating reports directly from R scripts</h1>
<p>For an introduction on creating nice reports directly from R scripts, look into the <a href="https://jozef.io/r909-rmarkdown-tips/#creating-beautiful-multi-format-reports-directly-from-r-scripts">relevant section</a> of the previous blog post. In one sentence, we can just call one of the following:</p>
<pre class="r"><code># with knitr directly
knitr::spin(&quot;path-to-r-script.R&quot;)

# or with rmarkdown
rmakdown::render(&quot;path-to-r-script.R&quot;)</code></pre>
<p>to create a report from an R script directly. Both <code>spin()</code> and <code>render()</code> provide a default style that will be used to render an R script to html. The same is true from RStudio’s built-in <code>File -&gt; Compile report...</code> functionality, which will call <code>render()</code> in the background when used.</p>
<blockquote>
<p>We might, however, be interested in using different styles other than the default one when rendering our R scripts into HTML reports, and there are multiple ways to achieve this.</p>
</blockquote>
<div id="including-styles-the-quick-dirty-and-risky-way" class="section level2">
<h2>Including styles the quick, dirty and risky way</h2>
<p>The fastest way to include a custom css stored in a file is to simply include a line like the following at the beginning of the R script that we are using <code>spin()</code> on:</p>
<pre class="r"><code>#&#39; &lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;path-to-our.css&quot;&gt;</code></pre>
<p>This simple approach however has many caveats, as the line is just inserted into the body of the document within a paragraph, completely oblivious to what else was inserted. Unless there is a very good reason, we should use one of the safer and more robust approaches mentioned below.</p>
</div>
</div>
<div id="using-knitrs-spin-directly" class="section level1">
<h1>Using knitr’s spin directly</h1>
<div id="under-the-spins-hood" class="section level2">
<h2>Under the spin’s hood</h2>
<p>Under the hood, <code>spin()</code> calls <code>knit2html()</code>, which passes many useful arguments to <code>markdownToHTML()</code>, the function that actually converts a markdown file to the final HTML format. Unfortunately, many of those useful arguments are not exposed via <code>spin()</code>.</p>
<p>Bearing this in mind, we have a few ways to access and provide them with the desired values:</p>
<ol style="list-style-type: decimal">
<li>Changing the options that govern the default values and just call <code>spin()</code> as before</li>
<li>Perform the spinning in 2 steps</li>
</ol>
</div>
<div id="changing-the-options-that-govern-the-default-values-and-just-call-spin-as-before" class="section level2">
<h2>Changing the options that govern the default values and just call <code>spin()</code> as before</h2>
<p>As mentioned above, <code>spin()</code> does not expose the arguments of <code>markdownToHTML()</code> directly, so what happens in practice is that the default values for those arguments are used when <code>spin()</code> is called. Some of the interesting arguments are by default selected in the following way:</p>
<pre class="r"><code>options = getOption(&quot;markdown.HTML.options&quot;), 
extensions = getOption(&quot;markdown.extensions&quot;) 
stylesheet = getOption(&quot;markdown.HTML.stylesheet&quot;)
template = getOption(&quot;markdown.HTML.template&quot;)</code></pre>
<p>Let’s have a look at some interesting default options’ values:</p>
<pre class="r"><code>library(markdown)
options()[c(
  &quot;markdown.HTML.options&quot;,
  &quot;markdown.extensions&quot;,
  &quot;markdown.HTML.stylesheet&quot;
)]</code></pre>
<pre><code>## $markdown.HTML.options
## [1] &quot;use_xhtml&quot;      &quot;smartypants&quot;    &quot;base64_images&quot;  &quot;mathjax&quot;       
## [5] &quot;highlight_code&quot;
## 
## $markdown.extensions
## [1] &quot;no_intra_emphasis&quot; &quot;tables&quot;            &quot;fenced_code&quot;      
## [4] &quot;autolink&quot;          &quot;strikethrough&quot;     &quot;lax_spacing&quot;      
## [7] &quot;space_headers&quot;     &quot;superscript&quot;       &quot;latex_math&quot;       
## 
## $markdown.HTML.stylesheet
## [1] &quot;/usr/local/lib/R/site-library/markdown/resources/markdown.css&quot;</code></pre>
<p>If we want to keep the spinning in one step, we can simply update those options before calling spin (and ideally change them back afterwards). For a somewhat minimalistic HTML output still keeping images self-contained, we can do:</p>
<pre class="r"><code>options(
  markdown.extensions = &quot;fenced_code&quot;,
  markdown.HTML.options = &quot;base64_images&quot;,
  markdown.HTML.stylesheet = &quot;{}&quot;
)
knitr::spin(&quot;spin_exaple.R&quot;)</code></pre>
<p>To use a custom css stylesheet instead of the one <a href="https://github.com/rstudio/markdown/blob/master/inst/resources/markdown.css">provided by default</a> with the markdown package:</p>
<pre class="r"><code>options(markdown.HTML.stylesheet = &quot;path_to_custom.css&quot;)
knitr::spin(&quot;path-to-r-script.R&quot;)</code></pre>
</div>
<div id="perform-the-report-creation-in-2-steps" class="section level2">
<h2>Perform the report creation in 2 steps</h2>
<p>The method above works but can seem quite workaround-ish. The method that could be considered more proper is to actually split the production of the final output into 2 steps:</p>
<ol style="list-style-type: decimal">
<li>Generate an intermediate .Rmd file via <code>spin()</code>, using <code>spin(..., knit = FALSE)</code></li>
<li>Run <code>knit2html()</code> on the created .Rmd file with the desired options directly specified as arguments</li>
</ol>
<p>This allows us to provide additional arguments <code>extensions</code>, <code>stylesheet</code>, <code>header</code>, <code>template</code> and <code>encoding</code> in the second step, instead of relying on the changed options to be passed as defaults.</p>
<p>The below example will embed styles present in <code>path_to_custom.css</code> into the resulting HTML:</p>
<pre class="r"><code># Creates the intermediate path-to-r-script.Rmd
knitr::spin(&quot;path-to-r-script.R&quot;, knit = FALSE)

# Now create the final HTML output from
# path-to-r-script.Rmd, with desired options
knitr::knit2html(
  input = &quot;path-to-r-script.Rmd&quot;,
  stylesheet = &quot;path_to_custom.css&quot;
)</code></pre>
<blockquote>
<p>Using both of the above options will actually embed the css directly into the HTML output that is produced, making the output larger in size.</p>
</blockquote>
<p>Note that the arguments we are looking to provide to <code>knit2html()</code> are implemented as part of <code>...</code>, so we will have to name them. To look at the details, study the <a href="https://www.rdocumentation.org/packages/markdown/versions/0.9/topics/markdownToHTML">documentation of <code>markdownToHTML()</code></a>, to which those arguments get passed.</p>
<div class="figure">
<img src="../img/r913-01-spin-with-css.png" alt="spin with custom air.css" />
<p class="caption">spin with custom <a href="https://github.com/markdowncss/air">air.css</a></p>
</div>
</div>
</div>
<div id="using-rmarkdowns-render" class="section level1">
<h1>Using rmarkdown’s render()</h1>
<p>To produce an HTML report from an R script we can also use <code>rmarkdown::render()</code> on an R script file. This will create a report with slight differences to the default <code>knit()</code> output, one notable for HTML output is that <code>render()</code> will by default include inline base64 representations of fonts and JavaScript sources. It will also include some potentially useful metadata, such as the author’s name and the date of rendering.</p>
<div id="the-output_format-powerhouse" class="section level2">
<h2>The output_format powerhouse</h2>
<p>The output of <code>render()</code> is governed mainly by the <code>output_format</code> argument. Most of the time users will pass on just the name of the format, such as <code>&quot;html_document&quot;</code>, as most of the options are governed by the yaml metadata present at the beginning of our Rmd files.</p>
<p>For R scripts we usually do not use the yaml metadata. In this case, we can take full advantage of the flexibility of that argument, passing a call to <code>rmarkdown::html_document()</code> with the desired parameters as <code>output_format</code>.</p>
</div>
<div id="minimalistic-output-with-render" class="section level2">
<h2>Minimalistic output with render()</h2>
<p>To produce a minimalistic HTML output from our <code>path-to-r-script.R</code> script, we can for example specify the following as <code>output_format</code>:</p>
<pre class="r"><code>rmarkdown::render(
  &quot;path-to-r-script.R&quot;, 
  output_format = rmarkdown::html_document(
    theme = NULL,
    mathjax = NULL,
    highlight = NULL
  )
)</code></pre>
</div>
<div id="custom-css-with-render" class="section level2">
<h2>Custom css with render()</h2>
<p>Including a custom css stylesheet is equally simple, just provide a <code>css</code> argument with the css file path to the <code>html_document()</code> function:</p>
<pre class="r"><code>rmarkdown::render(
  &quot;path-to-r-script.R&quot;, 
  output_format = rmarkdown::html_document(
    theme = NULL,
    mathjax = NULL,
    highlight = NULL,
    css = &quot;path_to_custom.css&quot;
  )
)</code></pre>
<p>An interesting property of including custom css styles is that by default the argument <code>self_contained</code> is set to <code>TRUE</code>, meaning that the full stylesheet will be embedded into the output HTML file, including all the external css imported into the one we are using. This means that if your stylesheets import external fonts such as the following, those will also be pasted directly into the output:</p>
<pre class="r"><code>@import url(http://fonts.googleapis.com/css?family=Open+Sans:300italic,300);</code></pre>
<p>This behavior is different for <code>spin()</code>, which will paste the <code>@import</code> clause into the output as-is, instead of parsing and pasting the actual content of the provided url.</p>
</div>
</div>
<div id="tldr-just-show-me-the-examples" class="section level1">
<h1>TL;DR: Just show me the examples</h1>
<p>If instead of reading about it you would like to just test it yourself, I created a very simple R project showcasing the mentioned methods and some more <a href="https://gitlab.com/jozefhajnala/gists/tree/master/rmarkdown/spin">available via a GitLab repo</a>.</p>
<p>The project has the following files:</p>
<ul>
<li><code>src/path-to-r-script.R</code> - an R script with custom formatted comments to be used as the source for creating reports with <code>knitr::spin()</code> and <code>rmarkdown::render()</code></li>
<li><code>rendering_render.R</code> - an R script that uses <code>rmarkdown::render()</code> to create multiple different output reports based on <code>path-to-r-script.R</code> and save them to <code>outputs/</code></li>
<li><code>rendering_spin.R</code> - an R script that uses <code>knitr::spin()</code> to create multiple different output reports based on <code>path-to-r-script.R</code> and save them to <code>outputs/</code></li>
<li><code>outputs/</code> - HTML reports generated from the content of <code>path-to-r-script.R</code> by running <code>rendering_spin.R</code> and <code>rendering_render.R</code></li>
<li><code>css/</code> - Example css used for creating <code>outputs/ex_04_spin_air_css.html</code>, all credit for the <code>air.css</code> goes to <a href="https://github.com/markdowncss/air">https://github.com/markdowncss/air</a></li>
</ul>
</div>
<div id="references" class="section level1">
<h1>References</h1>
<ul>
<li><a href="https://bookdown.org/yihui/rmarkdown/html-document.html">HTML document</a> chapter of the R Markdown: The Definitive Guide book</li>
<li><a href="https://jozef.io/r909-rmarkdown-tips/">Create R Markdown reports and presentations even better with these 3 practical tips</a></li>
<li><a href="https://github.com/markdowncss/air">air.css</a> style used to create the report on the screenshot above</li>
</ul>
</div>
