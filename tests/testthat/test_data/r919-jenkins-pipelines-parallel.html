---
title: "Using parallelization, multiple git repositories and setting permissions when automating R applications with Jenkins"
description: "In this post, we look at various tips that can be useful when automating R application testing and continuous integration, with regards to orchestrating parallelization, combining sources from multiple git repositories and ensuring proper access right to the Jenkins agent."
author: "Jozef Hajnala"
date: 2019-08-10T12:00:00Z
categories: ["vaRious"]
tags: ["r", "ci/cd", "jenkins"]
---



<div id="introduction" class="section level1">
<h1>Introduction</h1>
<p>In the <a href="https://jozef.io/r918-jenkins-pipelines/">previous post</a>, we focused on setting up declarative Jenkins pipelines with emphasis on parametrizing builds and using environment variables across pipeline stages.</p>
<blockquote>
<p>In this post, we look at various tips that can be useful when automating R application testing and continuous integration, with regards to orchestrating parallelization, combining sources from multiple git repositories and ensuring proper access right to the Jenkins agent.</p>
</blockquote>
</div>
<div id="contents" class="section level1">
<h1>Contents</h1>
<ol style="list-style-type: decimal">
<li><a href="#running-stages-in-parallel">Running stages in parallel</a>
<ol style="list-style-type: decimal">
<li><a href="#parallel-computation-using-r">Parallel computation using R</a></li>
<li><a href="#orchestrating-parallelization-of-r-jobs-with-jenkins">Orchestrating parallelization of R jobs with Jenkins</a></li>
<li><a href="#failing-early">Failing early</a></li>
</ol></li>
<li><a href="#cloning-multiple-git-repositories">Cloning multiple git repositories</a>
<ol style="list-style-type: decimal">
<li><a href="#cloning-into-a-separate-subdirectory">Cloning into a separate subdirectory</a></li>
<li><a href="#cleaning-up">Cleaning up</a></li>
</ol></li>
<li><a href="#changing-permissions-to-allow-the-jenkins-user-to-read">Changing permissions to allow the Jenkins user to read</a></li>
<li><a href="#references">References</a></li>
</ol>
</div>
<div id="running-stages-in-parallel" class="section level1">
<h1>Running stages in parallel</h1>
<div id="parallel-computation-using-r" class="section level2">
<h2>Parallel computation using R</h2>
<p>There are numerous way to achieve parallel computation in the context of an R application, those native to R are for example</p>
<ul>
<li>the <a href="https://stat.ethz.ch/R-manual/R-devel/library/parallel/doc/parallel.pdf">parallel package</a>, which is included with base R since version 2.14 and very stable, or</li>
<li>the more recent <a href="https://cran.r-project.org/package=future">future package</a></li>
<li>the CRAN Task View: <a href="https://cran.r-project.org/web/views/HighPerformanceComputing.html">High-Performance and Parallel Computing with R</a> provides a useful and extensive overview of multiple topics, including parallelism with R</li>
</ul>
<p>Governing parallelism directly within R code requires tackling many aspects, starting with logging and ending in handling conditions and exception. We might therefore also be interested in leaving the orchestration of parallelism to a layer above the R application code itself. This approach has both benefits and limitations, so careful consideration should be taken before the implementation starts.</p>
</div>
<div id="orchestrating-parallelization-of-r-jobs-with-jenkins" class="section level2">
<h2>Orchestrating parallelization of R jobs with Jenkins</h2>
<p>Declarative Jenkins pipelines are one of the ways to orchestrate parallelism with many options, a very simple example of a parallelized process can look as follows:</p>
<pre class="bash"><code>pipeline {
  agent any
    stages {
      
      stage(&#39;Preparation&#39;) {
        steps {
          // Cleanup, Environment setup, etc.
        }
      }
      
      stage(&#39;Tests&#39;) {
        parallel {
          stage(&#39;Unit Tests&#39;) {
            steps {
              // Invoke unit tests
            }
          }
          stage(&#39;Integration Tests&#39;) {
            steps {
              // Invoke integration tests
            }
          }
          stage(&#39;Regression Tests&#39;) {
            steps {
             // Invoke regression tests
            }
          }
          stage(&#39;Technical checks&#39;) {
            steps {
              // Invoke Technical checks
            }
          }
        }
      }
      
  }
}</code></pre>
<p>Note the <code>parallel</code> directive, which will ensure that the (sub)stages within it</p>
<ul>
<li>Unit Tests</li>
<li>Integration Tests</li>
<li>Regression Tests and</li>
<li>Technical checks</li>
</ul>
<p>will be executed in parallel.</p>
<blockquote>
<p>The parallelization will be orchestrated only after the first stage - “Preparation” was finished first. This is useful in case we need a stage that is shared among the parallel stages to be executed first.</p>
</blockquote>
</div>
<div id="failing-early" class="section level2">
<h2>Failing early</h2>
<p>If we want to fail the parallel stages early (as soon as any of them fails), we can add <code>failFast true</code> into the parallel stage:</p>
<pre class="bash"><code>stage(&#39;Tests&#39;) {
  failFast true
  parallel {
    // ...
  }
}</code></pre>
<div class="figure">
<img src="../img/r919-01-parallel-stages-blueocean.png" alt="An example parallel Jenkins pipeline shown by BlueOcean. Image credit https://bit.ly/31e8cAy" />
<p class="caption">An example parallel Jenkins pipeline shown by BlueOcean. Image credit <a href="https://bit.ly/31e8cAy" class="uri">https://bit.ly/31e8cAy</a></p>
</div>
</div>
</div>
<div id="cloning-multiple-git-repositories" class="section level1">
<h1>Cloning multiple git repositories</h1>
<p>In certain situations, we may need to clone not just the main repository that is subject to our multibranch pipeline, but also secondary repositories.</p>
<blockquote>
<p>An example of such setup is when we store modeling parameters for our run in a separate repository, or when configurations governing the runs are stored in a separate repository.</p>
</blockquote>
<p>The <code>git</code> directive allows us to clone another repository. Note that if you need to use credentials for the process, those are configured in <a href="https://jenkins.io/doc/book/using/using-credentials/#configuring-credentials">Jenkins’ credential configuration</a>.</p>
<pre class="bash"><code>stage(&#39;Clone another repository&#39;) {
  steps {
    git branch: &#39;master&#39;,
    credentialsId: &#39;my-credential-id&#39;,
    url: &#39;git@github.com:user/repo.git&#39;
  }
}</code></pre>
<div id="cloning-into-a-separate-subdirectory" class="section level2">
<h2>Cloning into a separate subdirectory</h2>
<p>Note however this will clone the repository into the current working directory, where the main repository subject to the pipeline is likely already checked out. This may have unintended consequences, so a safer approach is to checkout the secondary repository into a separate directory. We can achieve this using the <code>dir</code> directive:</p>
<pre class="bash"><code>stage(&#39;Clone another repository to subdir&#39;) {
  steps {
    sh &#39;rm subdir -rf; mkdir subdir&#39;
    dir (&#39;subdir&#39;) {
      git branch: &#39;master&#39;,
        credentialsId: &#39;my-credential-id&#39;,
        url: &#39;git@github.com:user/repo.git&#39;
    }
  }
}</code></pre>
</div>
<div id="cleaning-up" class="section level2">
<h2>Cleaning up</h2>
<p>After the pipeline is done, it may be useful do perform cleanup steps, for example removing unneeded directories. Since we likely want to clean those up regardless of the pipeline results, we can take advantage of the <code>post</code> directive running <code>always</code>, which will be executed regardless of the outcome of the pipeline stages.</p>
<p>One example use is to remove the hidden <code>.git</code> directories from both the working directory, where the main repository is checked out and the <code>&quot;subdir&quot;</code>, where we checked out the secondary repository:</p>
<pre class="bash"><code>post {
  always {
    sh &#39;rm .git -rf&#39;
    sh &#39;rm subdir/.git -rf&#39;
  }
}</code></pre>
</div>
</div>
<div id="changing-permissions-to-allow-the-jenkins-user-to-read" class="section level1">
<h1>Changing permissions to allow the Jenkins user to read</h1>
<p>One aspect of using Jenkins to execute our R code is to ensure that the Jenkins user executing the code on the worker node has access to all the necessary files. The following is a list of useful Linux commands that can help with the setup. These should, of course, be used with care.</p>
<pre class="bash"><code># Add user `jenkins` to group `somegroup`
usermod -a -G somegroup jenkins

# Change group of somedir/ to somegroup, recursively
chgrp -R somegroup somedir/

# Allow group to read `somedir`, recursively
chmod -R g+r somedir/

# Find all directories in a path and allow group to traverse
find /dir/moredir/somedir -type d -exec chmod g+x {} \;</code></pre>
</div>
<div id="references" class="section level1">
<h1>References</h1>
<ul>
<li>Jenkins documentation on <a href="https://jenkins.io/doc/book/pipeline/syntax/#parallel">parallel blocks</a></li>
<li>Jenkins documentation on <a href="https://jenkins.io/doc/book/using/using-credentials/#configuring-credentials">credential configuration</a></li>
<li>UnixExchange: <a href="https://unix.stackexchange.com/a/13891">Traversing directories</a></li>
<li>StackOverflow: <a href="https://stackoverflow.com/questions/38461705/checkout-jenkins-pipeline-git-scm-with-credentials">Checkout multiple git repos into same Jenkins workspace</a></li>
<li>StackOverflow: <a href="https://stackoverflow.com/questions/38461705/checkout-jenkins-pipeline-git-scm-with-credentials">Checkout Jenkins Pipeline Git SCM with credentials?</a></li>
</ul>
</div>
