# jhaddins

![pipeline](https://gitlab.com/jozefhajnala/jhaddins/badges/master/build.svg)

Welcome to `jhaddins`, a small R package with useful addins that make RStudio even better, with no dependencies on other packages.

## Why is this useful?

RStudio addins let you extend the functionality of RStudio with custom functions and map that functionality to a keyboard shortcut. Ever got tired of manually writing roxygen tags for your documentation? The addins let you do it with 1 key-press.

## What can it currently do?

The following addins are part of the package:

* `addRoxytagCode` - Adds roxygen tag code to current selections in the active RStudio document
* `addRoxytagLink` - Adds roxygen tag link to current selections in the active RStudio document
* `addRoxytagEqn` - Adds roxygen tag eqn to current selections in the active RStudio document
* `viewSelection` - View objects, files, functions and more in RStudio with 1 keypress
* `runProfiler` - Profile code selected in RStudio in the background, write results into a file and open it
* `runCurrentRscript` - Executes the currently open R script file in a separate session via Rscript with --vanilla option

## Installation

`devtools::install_git("https://gitlab.com/jozefhajnala/jhaddins")`

## How to get most of the addins?

The most efficient way to use the addins is to assign a keyboard shortcut to them. You can do this via `Tools -> Addins -> Browse Addins... -> Keyboard Shortcuts...` in RStudio.

## This sounds interesting, can I read more somewhere, even if I want to make my own addins?

I keep a [series of blog posts](http://jozefhajnala.gitlab.io/r/categories/rstudioaddins/) dedicated to the work on this package with some interesting information. Currently it has the following posts:

* [RStudio:addins part 1 - code reproducibility testing](https://jozefhajnala.gitlab.io/r/r101-addin-reproducibility/)
* [RStudio:addins part 2 - roxygen documentation formatting made easy](https://jozefhajnala.gitlab.io/r/r102-addin-roxytags/)
* [RStudio:addins part 3 - View objects, files, functions and more with 1 keypress](https://jozefhajnala.gitlab.io/r/r103-keypress-viewer/)
* [RStudio:addins part 4 - Unit testing coverage investigation and improvement, made easy](https://jozefhajnala.gitlab.io/r/r104-unit-testing-coverage/)
* [RStudio:addins part 5 - Profile your code on keypress in the background, with no dependencies](https://jozefhajnala.gitlab.io/r/r105-async-profiler/)


## Is there a resource directly from RStudio ?

Yes, RStudio has a very useful webinar on addins, you can [watch it here](https://www.rstudio.com/resources/webinars/understanding-add-ins/).
