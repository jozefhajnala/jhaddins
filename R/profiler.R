#' Profile an R expression
#'
#' @description Run the profiler to profile \code{expr}.
#'
#' @param expr \code{expression} to profile.
#'
#' @importFrom utils Rprof summaryRprof
#'
#' @examples \dontrun{
#'   profileExpression(expression(x <- runif(100000000)))
#' }
#'
#' @return \code{data.frame} with 2 columns: \code{totalTime} in
#'   seconds and \code{maxMemory} in MB.
profileExpression <- function(expr) {
  tmpF <- tempfile()
  on.exit({
    utils::Rprof(NULL)
    unlink(tmpF, force = TRUE)
  })
  if (!is.expression(expr)) {
    message("epxr must be an expression in profileExpression()")
    return(data.frame(
      totalTime = numeric(0),
      maxMemory = numeric(0)
    ))
  }
  gc()
  utils::Rprof(
    filename = tmpF,
    memory.profiling = TRUE,
    interval = 0.01,
    append = FALSE
  )
  evalRes <- try(eval(expr), silent = TRUE)
  utils::Rprof(NULL)
  if (inherits(evalRes, "try-error")) {
    return(data.frame(stringsAsFactors = FALSE,
                      totalTime = "EvalError",
                      maxMemory = "EvalError"
    ))
  }
  res <- utils::summaryRprof(filename = tmpF, memory = "both")
  data.frame(
    totalTime = max(res[["by.total"]][, 1L]),
    maxMemory = max(res[["by.total"]][["mem.total"]])
  )
}


#' Profile an R expression multiple times
#'
#' @description A wrapper around \code{\link{profileExpression}} to
#' profile an R expression multiple times or use a given time to
#' profile as many times as possible.
#'
#' @param expr \code{expression}, to be profiled.
#' @param times \code{integer(1)}, how many times to run the profiler.
#' @param maxtime \code{NULL or integer(1)}, time in seconds. If not
#'   \code{NULL}, the profiler will try to estimate how many times it
#'   can profile in \code{maxtime} and profile that many times,
#'   effectively attempting to profile as many times as possible in
#'   \code{maxtime} seconds, however at least 1 time always.
#'
#' @examples \dontrun{
#'   multiProfile(expression(x <- runif(100000000)), maxtime = 60L)
#' }
#'
#' @seealso \code{\link{profileExpression}}
#'
#' @return \code{data.frame} with 2 columns: \code{totalTime} in
#'   seconds and \code{maxMemory} in MB.
multiProfile <- function(
  expr,
  times = 10L,
  maxtime = getOption("jhaddins_profiler_maxtime", default = NULL)
){
  if (!(is.integer(times) || is.integer(maxtime))) {
    message("Either times or maxtime must be integer in multiProfile()")
    return(data.frame(
      totalTime = numeric(0),
      maxMemory = numeric(0)
    ))
  }

  first <- profileExpression(expr)
  if (!is.null(maxtime)) {
    if (is.numeric(first[["totalTime"]])) {
      times <- floor(maxtime / first[["totalTime"]])
    } else {
      message("Eval failed, cannot compute times from maxtime.")
      return(first)
    }
  }
  if (times <= 1L) {
    return(first)
  }
  rest <- do.call(
    rbind,
    lapply(rep(list(expr), times - 1L), profileExpression)
  )
  rbind(first, rest)
}


#' Write profiling results to a text file
#'
#' @param df \code{data.frame} to write.
#' @param filePath \code{character(1)} file name to write
#'   \code{df} into.
#'
#' @importFrom utils write.table
#'
#' @seealso
#'   \code{\link{profileExpression}}
#'   \code{\link{multiProfile}}
#'
#' @return Side-effects.
writeProfileDf <- function(df, filePath = tempfile()) {
  df <- as.data.frame(
    stringsAsFactors = FALSE,
    lapply(
      df,
      format,
      width = 9L, digits = 2L, nsmall = 2L, scientific = FALSE
    )
  )
  write.table(
    x = df,
    file = filePath,
    quote = FALSE,
    append = FALSE,
    sep = "\t",
    row.names = FALSE
  )
}


#' Profile code chunk selected in RStudio
#'
#' @description Intended to be used as an RStudio addin.
#'
#' @param inpContext \code{document_context} class object.
#' @param autoOpen \code{logical(1)} if \code{TRUE} the result
#'   file will be opened in RStudio.
#' @param wait \code{logical(1)} if \code{TRUE} the main R process
#'   will wait for the profiler to finish.
#'
#' @seealso
#'   \code{\link{profileExpression}}
#'   \code{\link{multiProfile}}
#'   \code{\link{writeProfileDf}}
#'
#' @return Side-effects caused by addin execution.
runProfiler <- function(
  inpContext = rstudioapi::getActiveDocumentContext(),
  autoOpen = TRUE,
  wait = FALSE
){

  tmpContext <- tempfile(fileext = ".rds")
  tmpRdata <- tempfile(fileext = ".RData")
  tmpR <- tempfile(fileext = ".R")
  tmpProf <- tempfile(fileext = ".txt")

  force(inpContext)
  saveRDS(inpContext, tmpContext)
  inpString <- inpContext[["selection"]][[1L]][["text"]]
  cat(inpString, file = tmpR)
  expr <- try(parse(file = tmpR), silent = TRUE)
  if (inherits(expr, "try-error")) {
    message("Selected text cannot be parsed, cannot profile.")
    unlink(tmpR, force = TRUE)
    return(1L)
  }
  save(
    list = ls(all.names = TRUE, envir = .GlobalEnv),
    file = tmpRdata,
    envir = .GlobalEnv
  )
  script <- paste(sep = "; ",
    paste0("try({ load('", tmpRdata, "')"),
    paste0("res <- jhaddins:::multiProfile(parse('", tmpR, "'))"),
    "jhaddins:::writeProfileDf(res) })",
    paste0("unlink('", tmpR, "', force = TRUE)"),
    paste0("unlink('", tmpRdata, "', force = TRUE)"),
    paste0("unlink('", tmpContext, "', force = TRUE)")
  )
  file.create(tmpProf)
  if (isTRUE(autoOpen)) {
    rstudioapi::navigateToFile(tmpProf)
  }

  if (isTRUE(wait)) {
    message("Profiler starting")
  } else {
    message("Profiler starting in the background")
  }
  system2(
    command = 'Rscript',
    args = c('-e', shQuote(script)),
    wait = wait
  )

  return(0L)
}
